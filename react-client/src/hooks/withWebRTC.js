import { useState, useRef } from 'react';
import { hasRTCPeerConnection } from '../utils/webrtc';
import { SIGNALLING_SERVER, GOOGLE_STUN } from '../urls';

function useWebRTC() {
  const [name, setName] = useState('');
  const [loginSuccessful, setLoginSuccessful] = useState(false);
  const [dataChannelOpen, setDataChannelOpen] = useState(false);
  const [currentMessage, setCurrentMessage] = useState(null);
  const connection = useRef(new WebSocket(SIGNALLING_SERVER));
  const sendChannel = useRef();
  const dataChannel = useRef();
  const yourConnection = useRef();
  const connectedUser = useRef(null);
  const canvas = useRef(null);

  connection.current.onopen = function () {
    console.log("Connected");
  };

  connection.current.onerror = function (err) {
    console.log("Got error", err);
  };

  // Handle all messages through this callback
  connection.current.onmessage = function (message) {
    console.log("Got message", message.data);
    var data = JSON.parse(message.data);
    switch(data.type) {
      case "login":
        onLogin(data.success);
        break;
      case "offer":
        onOffer(data.offer, data.name);
        break;
      case "answer":
        onAnswer(data.answer);
        break;
      case "candidate":
        onCandidate(data.candidate);
        break;
      case "leave":
        onLeave();
        break;
      default:
        break;
    }
  };

  // Alias for sending messages in JSON format
  function send(message) {
    if (connectedUser.current) {
      message.name = connectedUser.current;
    }

    connection.current.send(JSON.stringify(message));
  };

  // Login when the user clicks the button
  const handleLogin = (username) => {
    setName(username)
    if (username.length > 0) {
      send({
        type: "login",
        name: username,
      });
    }
  };

  function onLogin(success) {
    if (success === false) {
      setLoginSuccessful(false);
      alert("Login unsuccessful, please try a different name.");
    } else {
      setLoginSuccessful(true);
      // Get the plumbing ready for a call
      startConnection();
    }
  };

  function startConnection() {
    if (hasRTCPeerConnection()) {
      setupPeerConnection();
    } else {
      alert("Sorry, your browser does not support WebRTC.");
    }
  }

  function receiveChannelCallback(event) {
    sendChannel.current = event.channel;

    setupDataChannelEvents(sendChannel.current);
  }

  function setupCanvasStream() {
    canvas.current = document.querySelector('.canvas');
    const canvasStream = canvas.current.captureStream();

    canvasStream.getTracks().forEach(
      track => {
        yourConnection.current.addTrack(
          track,
          canvasStream,
        );
      }
    );
  }

  function setupPeerConnection() {
    var configuration = {
      "iceServers": [{ "urls": GOOGLE_STUN }]
    };
    yourConnection.current = new RTCPeerConnection(configuration);
    yourConnection.current.ondatachannel = receiveChannelCallback;

    openDataChannel();
    setupCanvasStream();

    // Setup ice handling
    yourConnection.current.onicecandidate = function (event) {
      if (event.candidate) {
        send({
          type: "candidate",
          candidate: event.candidate
        });
      }
    };
  }

  // Connection creation
  const createConnection = (username) => {
    if (username.length > 0) {
      startPeerConnection(username);
    }
  };

  function startPeerConnection(user) {
    connectedUser.current = user;

    // Begin the offer
    yourConnection.current.createOffer(function (offer) {
      send({
        type: "offer",
        offer: offer
      });
      yourConnection.current.setLocalDescription(offer);
    }, (error) => {
      alert("An error has occurred.", error);
    });
  };

  function onOffer(offer, name) {
    connectedUser.current = name;
    yourConnection.current.setRemoteDescription(new RTCSessionDescription(offer));
  
    yourConnection.current.createAnswer(function (answer) {
      yourConnection.current.setLocalDescription(answer);
      send({
        type: "answer",
        answer
      });
    }, (error) => {
      alert("An error has occurred", error);
    });
  };
  
  function onAnswer(answer) {
    yourConnection.current.setRemoteDescription(new RTCSessionDescription(answer));
  };

  function onCandidate(candidate) {
    yourConnection.current.addIceCandidate(new RTCIceCandidate(candidate));
  };
  
  const handleLeave = () => {
    send({
      type: "leave"
    });
    onLeave();
  };

  function onLeave() {
    connectedUser.current = null;
    yourConnection.current.close();
    yourConnection.current.onicecandidate = null;
    yourConnection.current.onaddstream = null;
    setupPeerConnection();
  };

  // Data channel
  function openDataChannel() {
    var dataChannelOptions = {
      reliable: false,
    };

    dataChannel.current = yourConnection.current.createDataChannel("dataChannel", dataChannelOptions);
    setupDataChannelEvents(dataChannel.current, true);
  }

  function setupDataChannelEvents(channel, hasConnected = false) {
    channel.onerror = function (error) {
      console.log("Data Channel Error:", error);
    };

    channel.onmessage = function (event) {
      setCurrentMessage(event.data);
    };

    channel.onopen = function () {
      if (hasConnected) {
        channel.send(name + " has connected.");
      }
      setDataChannelOpen(true);
    };

    channel.onclose = function () {
      console.log("The Data Channel is Closed");
      setDataChannelOpen(false);
    };
  }

  const sendMessage = (val) => {
    dataChannel.current.send(val);
  };

  return {
    handleLogin,
    createConnection,
    handleLeave,
    sendMessage,
    dataChannelOpen,
    currentMessage,
    loginSuccessful,
    name,
    connectedUser: connectedUser.current,
  }
}

export default useWebRTC;