import { useState, useEffect } from 'react';
import Button from '../common/Button';
import Canvas from '../Canvas';

import './styles.css';

const CanvasChat = ({
  createConnection,
  handleLeave,
  sendMessage,
  dataChannelOpen,
  currentMessage,
  name,
  connectedUser,
}) => {
  const [messageList, setMessageList] = useState([]);
  const [friendName, setFriendName] = useState('');
  const [chatMessage, setChatMessage] = useState('');

  useEffect(() => {
    if (currentMessage) {
      setMessageList([
        `${connectedUser}: ${currentMessage}`,
        ...messageList,
      ]);
    }
  }, [currentMessage]);

  useEffect(() => {
    if (dataChannelOpen) {
      setFriendName('');
    }
  }, [dataChannelOpen])

  const handleMessage = () => {
    sendMessage(chatMessage);
    setMessageList([
      `${name}: ${chatMessage}`,
      ...messageList
    ]);
    setChatMessage('');
  }
  
  const handleKeyUp = (e) => {
    if (e.keyCode === 13) {
      handleMessage();
    } else {
      setChatMessage(e.target.value);
    }
  }

  return (
    <div className="mainContent">
      <div><Canvas /></div>
      <div>
        <div className="chatArea">
          {messageList.map((message, idx) => (<div key={idx}>{message}</div>))}
        </div>
      </div>
      <div>
        <div className="connectContent">
          <label>
            Enter your friend's name and press connect:
          </label>
          <input
            type="text"
            value={friendName}
            name="friendName"
            onChange={({ target: { value }}) => setFriendName(value)}
            disabled={dataChannelOpen}
          />
        </div>
        <Button
          disabled={friendName === '' || dataChannelOpen}
          onClick={() => createConnection(friendName)}
        >
          Connect
        </Button>
        {dataChannelOpen ? <Button onClick={() => handleLeave()}>Disconnect</Button> : null}
      </div>
      <div>
        <label>
          Enter a message for your friend:
        </label>
        <input
          type="text"
          className="chatInput"
          value={chatMessage}
          onChange={({ target: { value }}) => setChatMessage(value)}
          onKeyUp={handleKeyUp}
          disabled={!dataChannelOpen}
        />
        <Button
          disabled={!dataChannelOpen || !chatMessage}
          onClick={handleMessage}
        >
          Send
        </Button>
      </div>
    </div>
  );
}

export default CanvasChat;