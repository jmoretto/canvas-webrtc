import { useState, useRef, useEffect } from 'react';
import Button from '../common/Button';
import ColorPicker from '../common/ColorPicker';
import './styles.css';

const Canvas = () => {
  const canvasRef = useRef(null);
  const canvasCtx = useRef(null);
  const [isDrawing, setIsDrawing] = useState(false);
  const [prevCoords, setPrevCoords] = useState({ x: 0, y: 0 });
  const [coords, setCoords] = useState({ x: 0, y: 0 });
  const [brushColor, setBrushColor] = useState('red');

  useEffect(() => {
    canvasCtx.current = canvasRef.current.getContext("2d");
  }, []);

  useEffect(() => {
    if (isDrawing) {
      canvasCtx.current.beginPath();

      canvasCtx.current.lineWidth = 1;
      canvasCtx.current.lineCap = 'round';
      canvasCtx.current.strokeStyle = `${brushColor}`;

      canvasCtx.current.moveTo(prevCoords.x, prevCoords.y);
      canvasCtx.current.lineTo(coords.x, coords.y);
      canvasCtx.current.closePath();

      canvasCtx.current.stroke();
    }
  }, [isDrawing, coords, prevCoords]);

  const handleBrushColor = (color) => {
    setBrushColor(color);
  }

  const setPosition = (e) => {
    if (!isDrawing && e.buttons === 1) {
      setIsDrawing(true);
    }
    const newCoords = { x: (e.clientX - canvasRef.current.offsetLeft)*canvasRef.current.width/canvasRef.current.clientWidth, y: (e.clientY - canvasRef.current.offsetTop)*canvasRef.current.height/canvasRef.current.clientHeight };
    setPrevCoords({ x: coords.x || newCoords.x, y: coords.y || newCoords.y });
    setCoords(newCoords);
  }

  const stopDrawing = () => {
    setIsDrawing(false);
  }

  const clearCanvas = () => {
    canvasCtx.current.clearRect(0, 0, canvasRef.current.width, canvasRef.current.height);
  }

  return (
    <div className="canvasContent">
      <div>
        Draw on the whiteboard below, just click and drag your mouse! It will appear on your friend's screen!
      </div>
      <canvas
        ref={canvasRef}
        className="canvas"
        onMouseMove={setPosition}
        onMouseUp={stopDrawing}
        onMouseDown={setPosition}
        onMouseOut={stopDrawing}
      />
      <div className="btnSection">
        <Button onClick={clearCanvas}>Clear</Button>
        <ColorPicker onChange={handleBrushColor}>Brush Color</ColorPicker>
      </div>      
    </div>
  );
}

export default Canvas;