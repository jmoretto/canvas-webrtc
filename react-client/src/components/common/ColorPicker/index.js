import { useState } from 'react';
import { ChromePicker } from 'react-color'
import Button from '../Button';
import './styles.css'

const ColorPicker = ({
  onChange,
  children,
  initialColor = 'red',
}) => {
  const [displayColorPicker, setDisplayColorPicker] = useState(false);
  const [currentColor, setCurrentColor] = useState(initialColor);

  const handleClick = () => {
    setDisplayColorPicker(!displayColorPicker);
  };

  const handleClose = () => {
    setDisplayColorPicker(false);
  };

  const handleChange = (color) => {
    setCurrentColor(color.hex);
    onChange(color.hex);
  }

  return (
    <div>
      <Button backgroundColor={currentColor} onClick={handleClick}>
        {children}
      </Button>
      { displayColorPicker ? (
      <div className="popover">
        <div className="cover" onClick={handleClose}/>
        <ChromePicker color={currentColor} onChange={handleChange} />
      </div>) : null}
    </div>
  )
}

export default ColorPicker;