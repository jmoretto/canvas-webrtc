import './styles.css';

const Button = ({
  onClick,
  className,
  children,
  disabled,
  backgroundColor,
}) => {
  return (
    <button
      className={`btn ${className}`}
      onClick={onClick}
      disabled={disabled}
      type="button"
      style={backgroundColor ? { backgroundColor } : undefined}
    >
      {children}
    </button>
  );
}

export default Button;