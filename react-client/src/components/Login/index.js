import { useState } from 'react';
import Button from '../common/Button';
import './styles.css';

const Login = ({ handleLogin }) => {
  const [name, setName] = useState('');

  return (
    <div className="main">
      <div className="inputContent">
        <label htmlFor="name">
          Enter your name below!
        </label>
        <input type="text" name="name" className="inputLogin" onChange={({ target: { value }}) => setName(value)} />
      </div>
      <Button disabled={name === ''} onClick={() => handleLogin(name)}>Login</Button>
    </div>
  );
}

export default Login;