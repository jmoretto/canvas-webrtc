import './App.css';
import Login from './components/Login';
import CanvasChat from './components/CanvasChat';
import useWebRTC from './hooks/withWebRTC';

function App() {
  const {
    handleLogin,
    createConnection,
    handleLeave,
    sendMessage,
    dataChannelOpen,
    currentMessage,
    loginSuccessful,
    name,
    connectedUser,
  } = useWebRTC();

  return (
    <div className="App">
      {loginSuccessful ? (
        <CanvasChat
          createConnection={createConnection}
          handleLeave={handleLeave}
          sendMessage={sendMessage}
          dataChannelOpen={dataChannelOpen}
          currentMessage={currentMessage}
          name={name}
          connectedUser={connectedUser}
        />
      ) : (
        <Login handleLogin={handleLogin} />
      )}
    </div>
  );
}

export default App;
