var name, connectedUser;
var connection = new WebSocket('ws://localhost:8888');

connection.onopen = function () {
  console.log("Connected");
};

// Handle all messages through this callback
connection.onmessage = function (message) {
  console.log("Got message", message.data);
  var data = JSON.parse(message.data);
  switch(data.type) {
    case "login":
      onLogin(data.success);
      break;
    case "offer":
      onOffer(data.offer, data.name);
      break;
    case "answer":
      onAnswer(data.answer);
      break;
    case "candidate":
      onCandidate(data.candidate);
      break;
    case "leave":
      onLeave();
      break;
    default:
      break;
  }
};

connection.onerror = function (err) {
  console.log("Got error", err);
};

// Alias for sending messages in JSON format
function send(message) {
  if (connectedUser) {
    message.name = connectedUser;
  }

  connection.send(JSON.stringify(message));
};


var loginPage = document.querySelector('#login-page'),
    usernameInput = document.querySelector('#username'),
    loginButton = document.querySelector('#login'),
    callPage = document.querySelector('#call-page'),
    theirUsernameInput = document.querySelector('#their-username'),
    callButton = document.querySelector('#call'),
    sendButton = document.querySelector('#send'),
    received = document.querySelector('#received'),
    messageInput = document.querySelector('#message'),
    hangUpButton = document.querySelector('#hang-up');
callPage.style.display = "none";

// Login when the user clicks the button
loginButton.addEventListener("click", function (event) {
  name = usernameInput.value;
  if (name.length > 0) {
    send({
      type: "login",
      name,
    });
  }
});

function onLogin(success) {
  if (success === false) {
    alert("Login unsuccessful, please try a different name.");
  } else {
    loginPage.style.display = "none";
    callPage.style.display = "block";
    startConnection();
  }
};


// Connection setup
var yourVideo = document.querySelector('#yours'),
    theirVideo = document.querySelector('#theirs'),
    yourConnection, connectedUser, stream;
function startConnection() {
  if (hasRTCPeerConnection()) {
    setupPeerConnection();
  } else {
    alert("Sorry, your browser does not support WebRTC.");
  }
}

var sendChannel;

function receiveChannelCallback(event) {
  sendChannel = event.channel;

  setupDataChannelEvents(sendChannel);
}

function setupPeerConnection(stream) {
  var configuration = {
    "iceServers": [{ "urls": "stun:stun.1.google.com:19302" }]
  };
  yourConnection = new RTCPeerConnection(configuration);
  yourConnection.ondatachannel = receiveChannelCallback;

  openDataChannel();

  // Setup stream listening
  yourConnection.onaddstream = function (e) {
    theirVideo.src = window.URL.createObjectURL(e.stream);
  };
  yourConnection.ontrack = function (e) {
    if (theirVideo.srcObject !== e.streams[0]) {
      theirVideo.srcObject = e.streams[0];
      console.log('received remote track');
    }
  };;

  // Setup ice handling
  yourConnection.onicecandidate = function (event) {
    if (event.candidate) {
      send({
        type: "candidate",
        candidate: event.candidate
      });
    }
  };
}

function hasUserMedia() {
  navigator.getUserMedia = navigator.getUserMedia ||
  navigator.webkitGetUserMedia || navigator.mozGetUserMedia ||
  navigator.msGetUserMedia;
  return !!navigator.getUserMedia;
}

function hasRTCPeerConnection() {
  window.RTCPeerConnection = window.RTCPeerConnection ||
  window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
  window.RTCSessionDescription = window.RTCSessionDescription ||
  window.webkitRTCSessionDescription ||
  window.mozRTCSessionDescription;
  window.RTCIceCandidate = window.RTCIceCandidate ||
  window.webkitRTCIceCandidate || window.mozRTCIceCandidate;
  return !!window.RTCPeerConnection;
}


// Connection creation
callButton.addEventListener("click", function () {
  var theirUsername = theirUsernameInput.value;
  if (theirUsername.length > 0) {
    startPeerConnection(theirUsername);
  }
});

function startPeerConnection(user) {
  connectedUser = user;
  // Begin the offer
  yourConnection.createOffer(function (offer) {
    send({
      type: "offer",
      offer: offer
    });
    yourConnection.setLocalDescription(offer);
  },
  function (error) {
    alert("An error has occurred.");
  });
};

function onOffer(offer, username) {
  connectedUser = username;
  yourConnection.setRemoteDescription(new RTCSessionDescription(offer));

  yourConnection.createAnswer(function (answer) {
    yourConnection.setLocalDescription(answer);
    send({
      type: "answer",
      answer: answer
    });
  },
  function (error) {
    alert("An error has occurred");
  });
};

function onAnswer(answer) {
  yourConnection.setRemoteDescription(new RTCSessionDescription(answer));
};

function onCandidate(candidate) {
  yourConnection.addIceCandidate(new RTCIceCandidate(candidate));
};

hangUpButton.addEventListener("click", function () {
  send({
    type: "leave"
  });
  onLeave();
});

function onLeave() {
  connectedUser = null;
  theirVideo.src = null;
  yourConnection.close();
  yourConnection.onicecandidate = null;
  yourConnection.onaddstream = null;
  setupPeerConnection(stream);
};

var dataChannel;

// Data channel
function openDataChannel() {
  var dataChannelOptions = {
    reliable: false,
  };

  dataChannel = yourConnection.createDataChannel("myLabel", dataChannelOptions);
  setupDataChannelEvents(dataChannel, true);
}

function setupDataChannelEvents(channel, hasConnected = false) {
  channel.onerror = function (error) {
    console.log("Data Channel Error:", error);
  };

  channel.onmessage = function (event) {
    console.log("Got Data Channel Message:", event.data);
    received.innerHTML += "" + connectedUser + ": " + event.data + "<br />";
    received.scrollTop = received.scrollHeight;
  };

  channel.onopen = function () {
    if (hasConnected) {
      dataChannel.send(" has connected.");
    }
  };

  channel.onclose = function () {
    console.log("The Data Channel is Closed");
  };
}

sendButton.addEventListener("click", function (event) {
  var val = messageInput.value;
  received.innerHTML += "" + name + ": " + val + "<br />";
  received.scrollTop = received.scrollHeight;
  dataChannel.send(val);
});