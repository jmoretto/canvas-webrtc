This is a repository for streaming a canvas from one client to another. It uses a signalling server for WebRTC setup.

Technologies used:
 * React for frontend application
 * WebRTC for streaming data between two clients
 * Websockets for communicating between server and clients

This repo is split into three folders:
 * 'react-client': contains the react client for starting the canvas stream. This should always be the one to call on the other client or the canvas stream will not work.
 * 'js-client': contains the normal javascript client for receiving the canvas stream, this should not initiate the call with the react client.
 * 'server': Contains the WebRTC signalling server - communication is done through websockets.

The normal flow for this application should be:
* Start the signalling server
* Start the js-client and react-client
* Go to react-client page and login as a user
* Go the js-client page and login as another user
* On react-client enter the js-client username and click on connect
* The canvas on react-client should be mirrored by a video element on js-client
* The chat should also work normally between the two

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn server`

Launches the signalling server, this should be called first.

### `yarn js-client`

Launches a server using live-server that serves that js-client folder. This is used as the receiving end for the canvas stream.

### `Running the react-client application`
Go into the react-client application and run 'yarn start' 